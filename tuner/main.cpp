////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "tuner.h"

#include "movegen.h"
#include "fen.h"
#include "engine.h"
#include "board.h"
#include "util/logfile.h"

#include <iostream>
#include <string>
#include <vector>

using namespace Cheese;

////////////////////////////////////////////////////////////////////////////////
bool isPositionQuiet(ChessBoard &board)
{
	ChessMoveSortList movelist;
	ChessMoveGen::generateLegalMoves(board, movelist, board.inCheck());

	for (const auto &move : movelist) {
		const auto &m = move.move;
		if ((m.isCaptureOrPromotion()) &&
			(board.staticExchangeEvaluation(m) > 0)) {
			return false;
		}
	}
	
	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool filterPositions(const std::string &filename, const std::string &fileresult)
{
	ChessEngine::precalcTables();

	ChessBoard board;
	ChessFENParser parser(board);
	std::vector<std::string> fens;
	if (!parser.load(filename, fens)) {
		Logger::error() << "file not found : " << filename;
		return false;
	}

	std::ofstream ofs;
	ofs.open(fileresult, std::ofstream::out);

	std::size_t count = 0;
	for (const auto &f : fens) {

		ChessFENParser fenpos(board);
		if (!fenpos.set(f)) {
			Logger::warning() << "FEN parsing error : " << f;
			continue;
		}

		if (isPositionQuiet(board)) {
			ofs << f << '\n';
			++count;
		}
	}
	ofs.close();

	std::cout << "saved " << count << " / " << fens.size() 
			  << " positions" << std::endl;
	
	return true;
}

////////////////////////////////////////////////////////////////////////////////
int main(int argc, const char *argv[])
{
	std::vector<std::string> args(argv, std::next(argv, argc));
	if ((args.size() == 5) &&
		(args[1] == "-filter") &&
		(args[3] == "-out")) {
		return filterPositions(args[2], args[4]) ? 0 : 1;
	}
	
	Logger::open("logtuner.txt");
	Logger::enable_level(Logger::LogLevel::info, true);
	Logger::enable_level(Logger::LogLevel::warning, true);
	Logger::enable_level(Logger::LogLevel::error, true);
	Logger::enable_level(Logger::LogLevel::debug, Logger::use_log_debug);
		
	ChessTuner tuner;
	
	int r = (tuner.run() ? 0 : 1);
	
	Logger::info() << "End.";
	Logger::close();
	
	return r;
}

