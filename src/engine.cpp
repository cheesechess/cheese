////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "engine.h"

#include "bitbase.h"
#include "fen.h"
#include "moveorder.h"
#include "util/logfile.h"

#include "tablebases.h"

#include <cmath>
#include <iostream>
#include <limits>

namespace Cheese {

// default maximum number of moves in a game
constexpr int max_moves = 256;

// maximum number of moves in an opening book
constexpr int max_book_moves = 20;

constexpr std::size_t nb_elo_samples = 13;
using ELOArray = std::array<int, nb_elo_samples>;

const ELOArray elo_speed_values = { 50, 100, 500, 1000, 5000, 10000, 50000, 
100000, 250000, 500000, 1000000, 2000000, 3000000 };

const ELOArray elo_speed_ratings = { 974, 1026, 1397, 1616, 1947, 2113, 
2446, 2539, 2697, 2771, 2823, 2937, 2986 };

const ELOArray elo_nodes_values = { 50, 100, 500, 1000, 5000, 10000, 50000, 
100000, 250000, 500000, 1000000, 2000000, 3000000 };

const ELOArray elo_nodes_ratings = { 752, 753, 959, 1149, 1623, 1831, 2228, 
2404, 2598, 2744, 2850, 2853, 2868 };

// fix classic chess castling before sending to GUI
// the engine moves king to rook square for castling,
// but GUI wants normal castling moves
constexpr std::array<Square, nb_squares> castlefix = {
	c1, b1, c1, d1, e1, f1, g1, g1,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a7, b7, c7, d7, e7, f7, g7, h7,
	c8, b8, c8, d8, e8, f8, g8, g8
};

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::precalcTables()
{
	if constexpr (generate_zobrist_keys) {
		Zobrist::generate();
	}

	if constexpr (BitBases::generate_bitbase) {
		BitBases::generate();
	}

	// precalc bitboards tables
	BitBoards::init();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::init(int hashsize)
{
	Logger::debug() << "Engine init : " << hashsize;

	gameMoves.reserve(max_moves);
	undoMoves.reserve(max_moves);

	// set default parameters
	eval.initEvalData();

	repetition.clear();

	// init hash table
	hashTable.init(hashsize);

	Logger::info() << "Pawns hash table size : " << hashtable_pawn_size
				   << " Kb by thread";

	threads.create(*this);

	setStartPosition();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::free()
{
	Logger::debug() << "Engine free";

	// stop search
	pondering = false;

	if (isThinking()) {
		stopThinking();
	}

	threads.destroy();
	hashTable.release();
}

////////////////////////////////////////////////////////////////////////////////
// linear interpolation between ratings/value samples
int calcStrengthLimit(int elo, const ELOArray &ratings, const ELOArray &values)
{
	const int vmin = values[0];
	const int vmax = values[values.size() - 1];

	if (elo <= ratings[0]) {
		return vmin;
	}
	if (elo >= ratings[ratings.size() - 1]) {
		return vmax;
	}

	std::size_t p2 = 0;
	for (auto p=1U; p<ratings.size(); ++p) {
		if ((elo > ratings[p - 1]) && (elo <= ratings[p])) {
			p2 = p;
			break;
		}
	}

	auto p1 = p2 - 1;

	double a = static_cast<double>(values[p2] - values[p1]) /
			   static_cast<double>(ratings[p2] - ratings[p1]);
	double b = static_cast<double>(values[p1]) - a * 
			   static_cast<double>(ratings[p1]);

	double v = std::clamp(a * elo + b, static_cast<double>(vmin), 
		static_cast<double>(vmax));

	return static_cast<int>(std::floor(v));
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setStrengthELO(int e)
{
	strengthELO = std::clamp(e, min_strength_elo, max_strength_elo);
	useCustomStrength = (strengthELO < max_strength_elo);

	eloNodesLimit = 0;
	reduceSpeedNodes = 0;

	if (strengthELO < max_strength_elo) {

		if (eloLimitMode == ELOLimitMode::nodes) {

			// limit nodes
			eloNodesLimit = calcStrengthLimit(e, elo_nodes_ratings, 
				elo_nodes_values);

			Logger::info() << "set engine strength to " << strengthELO
					       << " ELO (" << eloNodesLimit << " nodes)";
		} else {

			// init reduce speed
			reduceSpeedNodes = calcStrengthLimit(e, elo_speed_ratings, 
				elo_speed_values);

			Logger::info() << "set engine strength to " << strengthELO
					       << " ELO (" << reduceSpeedNodes << " node/s)";
		}

	} else {
		Logger::info() << "set engine strength to maximum";
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setEngineSearchMode(SearchMode type, int value1,
	[[maybe_unused]] int value2)
{
	searchMode = type;
	switch (type) {

		case	SearchMode::fixed_depth:
				thinkDepth = std::clamp(value1, 1, max_depth - 1);
				break;

		case	SearchMode::fixed_nodes:
				fixedNodesValue = std::max(0, value1);
				break;

		case 	SearchMode::fixed_time:
				fixedTimeValue = static_cast<std::uint64_t>(std::max(0, 
					value1));
				break;

		default:
				break;
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::newGame()
{
	board.clear();
	gameMoves.clear();
	undoMoves.clear();
	repetition.clear();
	movesOutOfBook = 0;
	pondering = false;
	timeGameLength = 5 * 60 * 1000;
	searchMode = SearchMode::fixed_time;
	thinkDepth = 8;
	ideepNodeCountTime = 0;
	remainingmoves = 30;
	timeIncrement[white] = 0;
	timeIncrement[black] = 0;
	ideepTimeByMove = timeGameLength / remainingmoves;
	fixedNodesValue = 2000;
	fixedTimeValue = 5000;

	setStartPosition();
	hashTable.clear();
	resetTime();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::errorMoveFromGUI(const std::string &text)
{
	std::string str = "Illegal move: " + text;
	onSendInfoMessage(str);
	Logger::error() << str;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::setMoveSquareList(const std::string &text)
{
	std::size_t p = 0;
	while (p < text.size()) {

		File sx = file_a;
		Rank sy = rank_1;
		File dx = file_a;
		Rank dy = rank_1;
		Piece promotion = no_piece;

		// 'O-O' & 'O-O-O'
		if (((p + 2) < text.size()) &&
			(text[p] == 'O') &&
			(text[p + 1] == '-') &&
			(text[p + 2] == 'O')) {

			sx = squareFile(board.squareKingStart(board.side()));
			sy = (board.side() == white) ? rank_1 : rank_8;
			dy = sy;

			if (((p + 4) < text.size()) &&
				(text[p + 3] == '-') &&
				(text[p + 4] == 'O')) {
				dx =  squareFile(board.squareRookLong(board.side()));
				p += 5;
			} else {
				dx =  squareFile(board.squareRookShort(board.side()));
				p += 3;
			}

		} else
		if ((p + 3) < text.size()) {

			sx = static_cast<File>(text[p++] - 'a');
			sy = static_cast<Rank>(text[p++] - '1');
			dx = static_cast<File>(text[p++] - 'a');
			dy = static_cast<Rank>(text[p++] - '1');

			if ((sx < file_a) || (sx > file_h) ||
				(sy < rank_1) || (sy > rank_8) ||
				(dx < file_a) || (dx > file_h) ||
				(dy < rank_1) || (dy > rank_8)) {
				errorMoveFromGUI(text);
				return false;
			}

			// promotion
			if (p < text.size()) {

				char c = text[p++];

				if (c == 'q') {
					promotion = queen;
					++p;
				} else {
					if (c == 'r') {
						promotion = rook;
						++p;
					} else {
						if (c == 'b') {
							promotion = bishop;
							++p;
						} else {
							if (c == 'n') {
								promotion = knight;
								++p;
							} else {
								if (c != ' ') {
									errorMoveFromGUI(text);
									return false;
								}
							}
						}
					}
				}
			}
		}

		// skip other spaces (sdhould not happen)
		while ((p < text.size()) && (text[p] == ' ')) {
			++p;
		}

		Square src = makeSquare(sx, sy);
		Square dst = makeSquare(dx, dy);
		Piece mv = board.piece(src);
		int ep = ((mv == pawn) && (board.piece(dst) == no_piece) &&
				  (sx != dx) && board.isEnPassant());
		Piece cap = (ep == 0) ? board.piece(dst) : pawn;

		int cast = 0;
		if (mv == king) {

			// classic rules : change castle move to "king capture rook"
			if (board.getChessRules() == ChessRules::classic) {
				if (board.side() == white) {
					if ((src == board.squareKingStart(white)) && (dst == g1)) {
						dst = board.squareRookShort(white);
					}
					if ((src == board.squareKingStart(white)) && (dst == c1)) {
						dst = board.squareRookLong(white);
					}
				} else {
					if ((src == board.squareKingStart(black)) && (dst == g8)) {
						dst = board.squareRookShort(black);
					}
					if ((src == board.squareKingStart(black)) && (dst == c8)) {
						dst = board.squareRookLong(black);
					}
				}
			}

			if ((src == board.squareKingStart(board.side())) &&
				(board.bitboardPiece(board.side(), rook) &
					BitBoards::square[dst]) &&
				((dst == board.squareRookShort(board.side())) ||
				 (dst == board.squareRookLong(board.side())))) {
				cast = 1;
				cap = no_piece;
			}
		}

		ChessMove move(src, dst, mv, cap, promotion, cast, ep);

		if (!playMove(move)) {
			errorMoveFromGUI(text);
			return false;
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::playMove(ChessMove move)
{
	const HashKey key = board.getHashKey();

	if (!board.validMove(move)) {
		return false;
	}

	ChessMoveRec rec;
	board.makeMove(move, rec);

	if (board.isPositionLegal()) {
		repetition.push(key);
		if (!move.isReversible()) {
			repetition.clear();
		}
		gameMoves.push_back(move);
		undoMoves.push_back(rec);
	} else {
		board.unMakeMove(move, rec);
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::takeBack()
{
	if (getGameMoveCount() > 0) {
		ChessMove &move = gameMoves.back();
		ChessMoveRec &rec = undoMoves.back();

		board.unMakeMove(move, rec);

		gameMoves.pop_back();
		repetition.pop();

		// rebuild repetitions
		repetition.clear();

		for (int n=0; n<getGameMoveCount(); n++) {
			repetition.push(undoMoves[n].hashKey);
			if (!gameMoves[n].isReversible()) {
				repetition.clear();
			}
		}

		return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
// calculate mate value in number of full moves
int ChessEngine::calcMateValue(int value)
{
	if (value <= min_score_mate) {
		return (mate_value + value) / 2;
	}
	if (value >= max_score_mate) {
		return (mate_value - value) / 2 + 1;
	}
	return value;
}

////////////////////////////////////////////////////////////////////////////////
// test : extract PV from hash table
void ChessEngine::extractPV(int depth)
{
	std::array<ChessMove, 32> pvmove;
	std::array<ChessMoveRec, max_depth> moveUndo;
	ChessMove cm;
	int pvdepth = depth - 1;
	int nbpv = 0;

	while (pvdepth >= 0) {

		cm.clear();

		if (!board.validMove(cm)) {
			break;
		}

		ChessMoveRec &rec = moveUndo[nbpv];

		board.makeMove(cm, rec);
		if (!board.isPositionLegal()) {
			break;
		}

		pvmove[nbpv++] = cm;

		--pvdepth;
	}

	for (pvdepth = (nbpv - 1); pvdepth>=0; pvdepth--) {
		ChessMoveRec &rec = moveUndo[pvdepth];
		board.unMakeMove(pvmove[pvdepth], rec);
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::initSearch()
{
	ideepMaxDepth = max_depth - 1;
	ideepNodeCheckTime = check_time_interval;
	ideepNodeCountTime = 0;
	ideepTimeByMove = fixedTimeValue;

	int tm = getTime(board.side());

	// init time management and stop conditions
	switch (searchMode) {

		case	SearchMode::tournament:

				if (remainingmoves != 0) {
					ideepTimeByMove = tm / std::max(remainingmoves, 2);
				} else {
					ideepTimeByMove = tm / 30;
				}

				ideepTimeByMove += timeIncrement[board.side()];

				// give more time for the 10 first moves out of book
				// (first move = x2 time)
				if (movesOutOfBook <= 9) {
					double r = 2.0 - movesOutOfBook / 10.0;
					ideepTimeByMove = static_cast<std::uint64_t>(
						static_cast<double>(ideepTimeByMove) * r);
				}
				break;

		case	SearchMode::fixed_depth:
				ideepMaxDepth = thinkDepth;
				break;

		case	SearchMode::fixed_nodes:
				if (ideepNodeCheckTime > fixedNodesValue) {
					ideepNodeCheckTime = fixedNodesValue;
				}
				break;

		case	SearchMode::fixed_time:
				ideepTimeByMove = fixedTimeValue;
				break;

		case	SearchMode::infinite_search:
				ideepTimeByMove = std::numeric_limits<std::uint64_t>::max();
				break;

		default:
			ideepTimeByMove = fixedTimeValue;
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::startSearch(bool ponder)
{
	Logger::debug() << "startSearch : move " << getGameMoveCount() 
		<< " remaining " << remainingmoves;

	threads.waitEndSearch();

	// search start time
	timerSearch.start();

	pondering = ponder;

	threads.resetNodeCount();

	// wake up the main thread to start the search
	threads.getMainThread()->thinking = true;
	threads.getMainThread()->notify();
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::search(ChessMoveSortList &moves)
{
	// engine side
	engineSide = board.side();
	eval.setEngineSide(engineSide);

	// generate legal moves in current position
	bool incheck = board.inCheck();

	moves.clear();
	int countMovesRoot = ChessMoveGen::generateLegalMoves(board,
			moves, incheck);

	// no legal moves : should never happen
	if (countMovesRoot == 0) {
		onEndSearch(ChessMove(0), ChessMove(0), 0, 0, 0ULL, 0);
		return false;
	}

	ChessMove bestmove(0);
	ChessMove ponderMove(0);

	// search in opening book
	if (useOpeningBook &&
		(!pondering) &&
		(getGameMoveCount() < max_book_moves) &&
		(searchMode != SearchMode::infinite_search)) {

		bestmove = openingBook.findMove(board);

		if (!bestmove.isEmpty()) {
			// test if move is valid
			bool tstbook = false;
			for (int n=0; n<countMovesRoot; n++) {
				if (moves[n].move == bestmove) {
					tstbook = true;
					break;
				}
			}

			if (tstbook) {

				Logger::debug() << "opening book move : " 
							    << bestmove.textMove();

				onEndSearch(bestmove, ponderMove,
					1, 1, 0ULL, 0);
				pondering = false;
				return false;
			} else {
				Logger::warning() << "invalid book move : " << bestmove.text();
				bestmove.clear();
			}
		}
	}

	TBCardinality = std::min(TBProbeLimit, TableBases::largest());
	if (TBCardinality > 0) {

		int nbp = BitBoards::bitCount(board.bitboardAll());
		if ((nbp <= TBCardinality) &&
			(!board.canCastle(castle_init))) {

			int score = 0;
			int r = TableBases::probe_root(board, score, moves);
	
			if (r != 0) {
				moves.sort(0, moves.size());				
				threads.getMainThread()->tbhits += moves.size();
				TBCardinality = 0;
			}
		}
	}

	if (!pondering) {
		++movesOutOfBook;
	}

	// init time management and stop conditions
	initSearch();

	// update main hashtable age
	hashTable.initSearch();

	threads.initSearch();

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::reduceSpeed()
{
	if (threads.mustStop()) {
		return;
	}

	std::uint64_t elapsed = getSearchElapsedTime();
	std::uint64_t sp = (getSearchNodeCount() * 1000) / (elapsed + 1);

	if (sp > getReduceSpeedNodes()) {

		// wait if we have enough time
		if ((elapsed + (100 + 16)) < ideepTimeByMove) {
			Timer::sleep(100);
		}

		// force to check time
		ideepNodeCountTime = ideepNodeCheckTime;
	}
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::checkSearchTime()
{
	if (threads.mustStop()) {
		return false;
	}

	++ideepNodeCountTime;
	if (ideepNodeCountTime < ideepNodeCheckTime) {
		return true;
	}

	ideepNodeCountTime = 0;

	// in pondering : don't stop searching
	if ((!pondering) &&
		(searchMode != SearchMode::fixed_depth)) {

		if (searchMode != SearchMode::fixed_nodes) {
			// compare to time by move
			auto diff = timerSearch.getElapsedTime();
			if (diff >= ideepTimeByMove) {
				threads.stopSearch();
				return false;
			}
		} else {
			std::uint64_t nodes = threads.getNodeCount();
			if (nodes >= fixedNodesValue) {
				if ((nodes + ideepNodeCheckTime) > fixedNodesValue) {
					ideepNodeCheckTime = fixedNodesValue - nodes;
				}
				threads.stopSearch();
				return false;
			}
		}
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setPersonality(const std::string &fname)
{
	if (eval.loadPersonality(fname)) {
		//...
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::stopThinking()
{
	threads.stopSearch();

	// force to check time
	ideepNodeCountTime = ideepNodeCheckTime;

	threads.waitEndSearch();
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::ponderHit()
{
	pondering = false;
	ideepNodeCountTime = ideepNodeCheckTime;
	checkSearchTime();
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::checkNodes()
{
	std::uint64_t nodes = threads.getNodeCount();
	std::uint64_t maxnodes = (eloNodesLimit != 0) ? eloNodesLimit : 
		fixedNodesValue;
	// don't stop before reaching depth 1
	int depth = threads.getMainThread()->searchDepth;
	if ((depth > 0) && (nodes >= maxnodes)) {
		threads.stopSearch();
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::searchingRootMove(ChessMove &mv, int num)
{
	onSearchRootMove(mv, num);
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setStartPosition()
{
	gameMoves.clear();
	undoMoves.clear();
	repetition.clear();
	board.setStartPosition();
}

////////////////////////////////////////////////////////////////////////////////
// set FEN (Forsythe-Edwards Notation)
bool ChessEngine::setFENPosition(const std::string &str)
{
	gameMoves.clear();
	undoMoves.clear();
	repetition.clear();

	ChessFENParser fen(board);
	return fen.set(str);
}

////////////////////////////////////////////////////////////////////////////////
// get FEN (Forsythe-Edwards Notation)
std::string ChessEngine::getFENPosition(ChessBoard &pos) const
{
	ChessFENParser fen(pos);
	return fen.get();
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessEngine::getTextMove(const ChessMove &move) const
{
	Square src = move.src();
	Square dst = move.dst();

	if ((move.isCastle()) &&
		(board.getChessRules() == ChessRules::classic)) {
		dst = castlefix[dst];
	}

	std::string str = ChessMove::strSquare(src) + ChessMove::strSquare(dst);

	Piece pp = move.promotion();
	if (pp != no_piece) {
		str += pieces_name[pp];
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessEngine::getTextPV(const ChessPVMoveList &mlist, 
	int count) const
{
	std::string str;
	for (int n=0; n<count; n++) {

		str += getTextMove(mlist[n]);

		if (n < (count - 1)) {
			str += ' ';
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessEngine::loadOpeningBook(const std::string &filebook)
{
	useOpeningBook = openingBook.open(filebook);
	return useOpeningBook;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::unloadOpeningBook()
{
    useOpeningBook = false;
	openingBook.close();
}

////////////////////////////////////////////////////////////////////////////////
int ChessEngine::countLegalMoves(ChessBoard &pos) const
{
	return ChessMoveGen::countLegalMoves(pos);
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::setSMPMode(SMPMode m)
{
	smpmode = m;
	switch (smpmode) {
		case SMPMode::lazy:
			Logger::info() << "using Lazy SMP";
			break;
		case SMPMode::ybwc:
			Logger::info() << "using YBWC";
			break;
		default:
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::resetTime()
{
	timeGame[white] = timeGameLength;
	timeGame[black] = timeGameLength;
}

////////////////////////////////////////////////////////////////////////////////
void ChessEngine::showBookMoves()
{
	if (useOpeningBook) {
		openingBook.showMoves(board);
	} else {
		std::cout << "book not loaded" << std::endl;
	}
}

} // namespace Cheese
