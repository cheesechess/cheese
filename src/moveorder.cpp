////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "moveorder.h"

#include "engine.h"
#include "util/logfile.h"

#include <array>

#include <cassert>

namespace Cheese {

// sort moves values
const ChessMoveSort::value_type sortmove_hash = 100000;
const ChessMoveSort::value_type sortmove_capture_good = 80000;
const ChessMoveSort::value_type sortmove_killer1 = 70001;
const ChessMoveSort::value_type sortmove_killer2 = 70002;
const ChessMoveSort::value_type sortmove_history = 4000;
const ChessMoveSort::value_type sortmove_capture_bad = 2000;
const ChessMoveSort::value_type sortmove_default = 0;

// MVV-LVA table [captured][moved]
constexpr auto mvv_lva = []() {
		
	constexpr std::array<std::int32_t, nb_pieces_type> mvv_lva_pieces =
		{ 0, 1, 3, 3, 5, 9, 32 };

	constexpr std::int32_t scale_mvv_lva = 64;

	std::array<std::array<std::int32_t, nb_pieces_type>, nb_pieces_type> 
		table {};
	for (int y=0; y<nb_pieces_type; y++) {
		for (int x=0; x<nb_pieces_type; x++) {
			table[y][x] = (((y >= pawn) && (y <= king)) &&
						   ((x >= pawn) && (x <= king))) ?
				mvv_lva_pieces[y] * scale_mvv_lva - mvv_lva_pieces[x] : 0;
		}
	}	

	return table;
} ();

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMoves(const ChessBoard &board,
		ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		Piece cp = move.capture();
		Piece pp = move.promotion();

		auto score = sortmove_default;
		if ((cp != no_piece) || (pp == queen)) {

			Piece mp = move.piece();

			score = sortmove_capture_good + mvv_lva[cp][mp];

			assert((score >= sortmove_capture_good) &&
			       (score < sortmove_hash));

		} else
		if (move == moveKiller1) {
			score = sortmove_killer1;
		} else
		if (move == moveKiller2) {
			score = sortmove_killer2;
		} else {
			score = sortmove_history + history.get(move, board.side());

			assert((score >= sortmove_history) &&
				   (score < sortmove_killer2));
		}

		moves[n].setScore(score);
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesEvasion(const ChessBoard &board,
	ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		Piece cp = move.capture();
		Piece pp = move.promotion();

		auto score = sortmove_default;
		if ((cp != no_piece) || (pp == queen)) {

			Piece mp = move.piece();

			score = sortmove_capture_good + mvv_lva[cp][mp];

			assert((score >= sortmove_capture_good) &&
			       (score < sortmove_hash));

		} else {

			score = sortmove_history + history.get(move, board.side());

			assert((score >= sortmove_history) &&
				   (score < sortmove_killer2));
		}
		
		moves[n].setScore(score);
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesCaptures(ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		Piece cp = move.capture();
		Piece pp = move.promotion();

		auto score = sortmove_default;
		if ((cp != no_piece) || (pp == queen)) {

			Piece mp = move.piece();

			score = sortmove_capture_good + mvv_lva[cp][mp];

			assert((score >= sortmove_capture_good) &&
				   (score < sortmove_hash));

		} else {
			score = sortmove_history;
		}

		moves[n].setScore(score);
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesQuiet(const ChessBoard &board,
	ChessMoveSortList &moves, int pos, int nbmoves)
{
	for (int n=pos; n<(pos + nbmoves); n++) {
		moves[n].setScore(sortmove_history + 
			history.get(moves[n].move, board.side()));
	}

	moves.sort(pos, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
int ChessMoveOrder::orderMovesRoot(ChessSearch *search, ChessBoard &board,
	ChessMoveSortList &moves, int nbmoves, [[maybe_unused]] int nply,
	[[maybe_unused]] bool incheck)
{
	ChessMoveRec rec;
	for (int n=0; n<nbmoves; n++) {

		auto score = sortmove_default;

		// sort by quiescence
		if constexpr (sort_root_quiescence) {
			search->getRepetitions().push(board.getHashKey());
			board.makeMove(moves[n].move, rec);

			bool chk = board.inCheck();
			score = -search->searchQuiescent<NodeType::pv>(
				-mate_value, mate_value, depth_checks_quiescence, nply + 1, 
				chk);

			board.unMakeMove(moves[n].move, rec);
			search->getRepetitions().pop();

		} else {

			// sort by castling + captures/see + quiets sorted by eval
			Piece cp = moves[n].move.capture();
			Piece pp = moves[n].move.promotion();

			if ((cp != no_piece) || (pp == queen)) {

				Piece mp = moves[n].move.piece();
				std::int32_t sc = mvv_lva[cp][mp];

				if (piece_value_see[cp] < piece_value_see[mp]) {
					int see = board.staticExchangeEvaluation(moves[n].move);
					if (see < 0) {
						score = sortmove_capture_bad + sc;

						assert((score >= sortmove_capture_bad) && 
                               (score < sortmove_history));

					} else {
						score = sortmove_capture_good + sc;

						assert((score >= sortmove_capture_good) && 
							   (score < sortmove_hash));
					}
				} else {
					score = sortmove_capture_good + sc;

					assert((score >= sortmove_capture_good) && 
						   (score < sortmove_hash));
				}

			} else {
				if (moves[n].move.isCastle()) {
					score = sortmove_hash;
				} else {
					board.makeMove(moves[n].move, rec);
					int hh = -search->evaluate(-mate_value, mate_value);
						score = sortmove_history + hh;

					assert((score > sortmove_capture_bad) &&
						   (score < sortmove_killer2));

					board.unMakeMove(moves[n].move, rec);
				}
			}
		}

		moves[n].setScore(score);
	}

	moves.sort(0, nbmoves);

	return nbmoves;
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesQuiescent(ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		Piece cp = move.capture();
		Piece mp = move.piece();

		auto score = sortmove_default;
		if (piece_value_see[cp] >= piece_value_see[mp]) {

			score = sortmove_capture_good + mvv_lva[cp][mp];

			assert((score >= sortmove_capture_good) &&
				   (score < sortmove_hash));

		} else {
			score = sortmove_capture_bad;
		}

		moves[n].setScore(score);
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
void ChessMoveOrder::orderMovesQuiescentChecks(const ChessBoard &board,
	ChessMoveSortList &moves, int nbmoves)
{
	for (int n=0; n<nbmoves; n++) {

		const ChessMove &move = moves[n].move;

		Piece cp = move.capture();
		Piece pp = move.promotion();

		auto score = sortmove_default;
		if ((cp != no_piece) || (pp == queen)) {

			Piece mp = move.piece();

			score = sortmove_capture_good + mvv_lva[cp][mp];

			assert((score >= sortmove_capture_good) &&
				   (score < sortmove_hash));

		} else {
			score = sortmove_history + history.get(move, board.side());

			assert((score >= sortmove_history) &&
  				   (score < sortmove_killer2));
		}

		moves[n].setScore(score);
	}

	moves.sort(0, nbmoves);
}

////////////////////////////////////////////////////////////////////////////////
ChessMoveOrder::ChessMoveOrder(ChessHistory &h, ChessMoveSortList &moves, 
	MoveOrderPhase ph, ChessMove mh, ChessMove mk1, ChessMove mk2, 
	ChessMove mc) : history(h), movelist(moves), phaseNext(ph), 
	count(moves.size()), moveHash(mh), moveKiller1(mk1), moveKiller2(mk2), 
	moveCounter(mc)
{
}

////////////////////////////////////////////////////////////////////////////////
ChessMove ChessMoveOrder::getNextMove(const ChessBoard &board)
{
	int cnt = 0;

	switch (phaseNext) {

		case	MoveOrderPhase::hash:
				if (board.validMove(moveHash)) {
					phase = phaseNext++;
					return moveHash;
				}
				++phaseNext;					

				[[fallthrough]];

		case	MoveOrderPhase::init_capture:					
				position = 0;
				pos_capture = 0;
				movelist.clear();
				count = ChessMoveGen::generateCaptures(board, movelist);
				orderMovesCaptures(movelist, count);
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::next_capture:
				while (position < count) {
					ChessMove move = movelist[position++].move;
					if (move != moveHash) {
						// do SEE now, and push move at the end of the list
						// if see < 0
						if ((piece_value_see[move.capture()] <
							 piece_value_see[move.piece()]) 
							&& (board.staticExchangeEvaluation(move) < 0)) {
							// use begining of the list for bad captures
							// pos_capture will never overwrite not
							// tested captures
							movelist[pos_capture++].move = move;
						} else {
							phase = phaseNext;		
							return move;
						}
					}
				}
				++phaseNext;
				
				[[fallthrough]];

		case	MoveOrderPhase::killer1:					
				if ((moveKiller1 != moveHash) &&
					(board.validMove(moveKiller1))) {
					phase = phaseNext++;
					return moveKiller1;
				}
				++phaseNext;
				
				[[fallthrough]];

		case	MoveOrderPhase::killer2:
				if ((moveKiller2 != moveHash) &&
					(board.validMove(moveKiller2))) {
					phase = phaseNext++;
					return moveKiller2;
				}
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::countermove:
				if ((moveCounter != moveHash) &&
					(moveCounter != moveKiller1) &&
					(moveCounter != moveKiller2) &&
					(board.validMove(moveCounter))) {
					phase = phaseNext++;
					return moveCounter;
				}
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::init_quiet:					
				cnt = ChessMoveGen::generateQuiets(board, movelist);
				orderMovesQuiet(board, movelist, count, cnt);
				count += cnt;
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::next_quiet:
				while (position < count) {
					ChessMove move = movelist[position++].move;
					if ((move != moveHash)
						&& (move != moveKiller1)
						&& (move != moveKiller2)
						&& (move != moveCounter)) {
						phase = phaseNext;
						return move;
					}
				}
				position = 0;
				count = pos_capture;
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::next_bad_capture:
				if (position < count) {
					ChessMove move = movelist[position++].move;
					phase = phaseNext;
					return move;
				}
				break;

		case	MoveOrderPhase::hash_check:					
				if (board.validMove(moveHash)) {
					phase = phaseNext++;
					return moveHash;
				}
				++phaseNext;
				
				[[fallthrough]];

		case	MoveOrderPhase::init_check:					
				position = 0;
				movelist.clear();
				count = ChessMoveGen::generateKingEvasions(board, movelist);
				orderMoves(board, movelist, count);
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::next_check:
				while (position < count) {
					ChessMove move = movelist[position++].move;
					if (move != moveHash) {
						phase = phaseNext;
						return move;
					}
				} 
				break;

		case	MoveOrderPhase::init_quiescence_capture:
				position = 0;
				movelist.clear();
				count = ChessMoveGen::generateQuietCaptures(board, movelist);
				orderMovesQuiescent(movelist, count);
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::next_quiescence_capture:
				while (position < count) {
					ChessMove move = movelist[position].move;
					auto value = movelist[position++].getScore();
					if ((value > sortmove_capture_bad) ||
						(board.staticExchangeEvaluation(move) >= 0)) {
						phase = phaseNext;
						return move;
					}
				} 
				break;

		case	MoveOrderPhase::init_quiescence_capture_checks:
				position = 0;
				movelist.clear();
				count = ChessMoveGen::generateQuietCaptures(board, movelist);
				orderMovesQuiescent(movelist, count);
				++phaseNext;

				[[fallthrough]];

		case	MoveOrderPhase::next_quiescence_capture_checks:
				while (position < count) {
					ChessMove move = movelist[position].move;
					auto value = movelist[position++].getScore();
					if ((value > sortmove_capture_bad) ||
						(board.staticExchangeEvaluation(move) >= 0)) {
						phase = phaseNext;
						return move;
					}
				} 
				++phaseNext;
			
				[[fallthrough]];

		case	MoveOrderPhase::init_quiescence_check:
				position = 0;
				movelist.clear();
				count = ChessMoveGen::generateQuietChecks(board, movelist);
				++phaseNext;

				[[fallthrough]];
				
		case	MoveOrderPhase::next_quiescence_check:
				while (position < count) {
					ChessMove move = movelist[position++].move;
					if (board.staticExchangeEvaluation(move) >= 0) {
						phase = phaseNext;
						return move;
					}
				}
				break;

		case	MoveOrderPhase::init_quiescence_evasion:
				position = 0;
				movelist.clear();
				count = ChessMoveGen::generateQuietKingEvasions(board,
					movelist);
				orderMovesQuiescentChecks(board, movelist, count);
				++phaseNext;

			    [[fallthrough]];

		case	MoveOrderPhase::next_quiescence_evasion:
		case	MoveOrderPhase::root:
				if (position < count) {
					phase = phaseNext;
					return movelist[position++].move;
				}
				break;

		default:
			break;
	}

	phaseNext = MoveOrderPhase::end;
	phase = phaseNext;
	return ChessMove(0);
}

} // namespace Cheese
