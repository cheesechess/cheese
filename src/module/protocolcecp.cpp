////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "protocolcecp.h"

#include "util/logfile.h"
#include "board.h"
#include "engine.h"
#include "module.h"
#include "tablebases.h"

#include <iomanip>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
ChessProtocolCECP::ChessProtocolCECP(ChessEngine &e, ChessModule &m) :
	ChessProtocol(e, m)
{
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::init()
{
	mainModule.remainingMoves = 30;
	mainModule.maxGameMoves = 30;
	countHalfMove = 0;
	firstThink = false;
	forcemode = false;

	mainModule.initEngine();
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::initFeatures()
{
	mainModule.sendCommand("feature myname=\"" +
					   ChessModule::name_program + ' ' +
					   std::string(Cheese::project_ver));

	mainModule.sendCommand("feature setboard=1");
	mainModule.sendCommand("feature analyze=1");
	mainModule.sendCommand("feature ping=1");
	mainModule.sendCommand("feature draw=0");
	mainModule.sendCommand("feature sigint=0");
	mainModule.sendCommand("feature sigterm=0");
	mainModule.sendCommand("feature colors=0");
	mainModule.sendCommand("feature smp=1");

	// supported chess variants
	mainModule.sendCommand("feature variants=\"normal,fischerandom\"");

	// gui options
	mainModule.sendCommand("feature option=\"Clear Hash -button\"");

	mainModule.sendCommand("feature done=1");
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolCECP::parse(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return true;
	}

	// move
	if (((token.size() == 4) || (token.size() == 5)) &&
		(token[0] >= 'a') && (token[0] <= 'h') &&
		(token[1] >= '1') && (token[1] <= '8') &&
		(token[2] >= 'a') && (token[2] <= 'h') &&
		(token[3] >= '1') && (token[3] <= '8')) {

		parseMove(token);

	} else
	if (token == "time") {

		if (nextToken(stream, token)) {
            // we are 1-player, because we have not played the opponent move
			engine.setPlayerTime(~engine.side(), 
				ChessModule::parseInt(token) * 10);
		}

	} else
	if (token == "otim") {

	    if (nextToken(stream, token)) {
			engine.setPlayerTime(engine.side(), 
				ChessModule::parseInt(token) * 10);
		}

	} else
	if (token == "go") {

        // first think
        if (!firstThink) {

            firstThink = true;

            // if number of moves is fixed : count moves already played in
			// the opening book
            if (mainModule.remainingMoves != 0) {
                mainModule.remainingMoves -= (countHalfMove / 2);
                engine.setRemainingMoves(mainModule.remainingMoves);
            }
        }

        if (forcemode) {
            forcemode = false;
        }

		mainModule.startSearch(false);

	} else
	if (token == "quit") {
		return false;
	} else
	if (token == "undo") {

		engine.takeBack();

	} else
	if (token == "force") {

		forcemode = true;

	} else
	if (token == "result") {

	    if (engine.isThinking()) {
			engine.stopThinking();
		}

	} else
	if (token == "analyze") {
		engine.setEngineSearchMode(SearchMode::infinite_search, 0, 0);
		mainModule.startSearch(false);
	}
	else
	if (token == "exit") {
	    // stop thinking (if we are pondering, the player played the wrong move)
		if (engine.isThinking()) {
			engine.stopThinking();
		}
	} else
	if (token == "setboard") {
        if (streamString(stream, token)) {
			// warning : if sent in game, look at current game move count
			// (used by chessgui for book moves...)
            if (!engine.setFENPosition(token)) {
				Logger::debug() << "Bad FEN string !";
				return false;
			}
		}
	} else
	if (token == "?") {

		// stop thinking (if we are pondering, the player played the wrong move)
		if (engine.isThinking()) {
			engine.stopThinking();
		}

	} else
	if (token == "new") {

		mainModule.ponderingActive = false;
		mainModule.movePonder.clear();

		if (engine.isThinking()) {
			engine.stopThinking();
		}
		engine.newGame();
        mainModule.remainingMoves = 30;
        mainModule.maxGameMoves = 30;
        countHalfMove = 0;
        firstThink = false;
        engine.setRemainingMoves(mainModule.remainingMoves);

		forcemode = false;

	} else
	if (token == "easy") {

		// stop current search
        if (engine.isThinking()) {
            engine.stopThinking();
        }

        // turn off pondering
		mainModule.ponderingActive = false;
		mainModule.movePonder.clear();

	} else
	if (token == "hard") {

		// turn on pondering
		mainModule.ponderingActive = true;

	} else
	if (token == "post") {

		// turn on pondering output
		showthinking = true;

	} else
	if (token == "nopost") {

		// turn off pondering output
		showthinking = false;

	} else
	if (token == "protover") {

	    if (nextToken(stream, token)) {
			protocolversion = ChessModule::parseInt(token);
			// send features commands
			initFeatures();
		}

	} else
	if (token == "ping") {

		if (nextToken(stream, token)) {
			mainModule.sendCommand("pong " + token);
		}
	} else
	if (token == "st") {

	    if (nextToken(stream, token)) {
            engine.setEngineSearchMode(SearchMode::fixed_time,
				ChessModule::parseInt(token) * 1000, 0);
		}

	} else
	if (token == "sd") {

	    if (nextToken(stream, token)) {
            engine.setEngineSearchMode(SearchMode::fixed_depth,
				ChessModule::parseInt(token), 0);
		}

	} else
	if (token == "level") {

	    if (nextToken(stream, token)) {

			int p1 = ChessModule::parseInt(token);
			int p2 = 0;
			int p20 = 0;
			int p21 = 0;
			double p3 = 0.0;

			if (nextToken(stream, token)) {

				auto p = token.find_first_of(':');
				if (p != std::string::npos) {
					p20 = ChessModule::parseInt(token.substr(0, p));
					p21 = ChessModule::parseInt(token.substr(p + 1));
				} else {
					p20 = ChessModule::parseInt(token);
				}

				if (nextToken(stream, token)) {
					p3 = ChessModule::parseDouble(token);
				}
			}

			mainModule.maxGameMoves = p1;
			mainModule.remainingMoves = p1;
            engine.setRemainingMoves(p1);

			p2 = p20 * 60 + p21;

            engine.setPlayerTime(engine.side(), p2 * 1000);
            engine.setTimeIncrement(white, static_cast<int>(p3 * 1000));
            engine.setTimeIncrement(black, static_cast<int>(p3 * 1000));
			engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
		}

	} else
	if (token == "option") {
		parseOption(stream);
	} else
	if (!mainModule.parseModuleCommand(token, stream)) {
		Logger::debug() << "unknown parse command : " << token;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolCECP::parseOption(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return false;
	}

	if (token == "Clear") {

		if (!nextToken(stream, token)) {
			return false;
		}

		if (token == "Hash") {
			engine.clearHashTable();
		}

	} else {
		Logger::debug() << "unknown option : " << token;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolCECP::parseMove(const std::string &command)
{
	++countHalfMove;

	if ((mainModule.ponderingActive) &&
		(mainModule.pondernextmove) &&
		(!mainModule.movePonder.isEmpty())) {

		File fs = static_cast<File>(command[0] - 'a');
		Rank rs = static_cast<Rank>(command[1] - '1');
		File fd = static_cast<File>(command[2] - 'a');
		Rank rd = static_cast<Rank>(command[3] - '1');

		// same move : play
		if ((fs == squareFile(mainModule.movePonder.src())) &&
			(rs == squareRank(mainModule.movePonder.src())) &&
			(fd == squareFile(mainModule.movePonder.dst())) &&
			(rd == squareRank(mainModule.movePonder.dst()))) {

			if (engine.isThinking()) {
				mainModule.pondernextmove = false;
				engine.ponderHit();
			} else {
				if (!engine.setMoveSquareList(command)) {
					return false;
				}
				mainModule.pondernextmove = true;
				mainModule.startSearch(false);
			}
			return true;

		} else {

			pondernewmove = command;

			if (engine.isThinking()) {
				engine.stopThinking();
			} else {
				if (!engine.setMoveSquareList(command)) {
					return false;
				}
				mainModule.pondernextmove = false;
				mainModule.startSearch(false);
			}
			return true;
		}

	} else {
		// play move
		if (!engine.setMoveSquareList(command)) {
			return true;
		}
	}

	// return if no moves possibles (mate)
	if (engine.countLegalMoves(engine.getBoardRef()) == 0) {
		return false;
	}

	// computer must play
	if (!forcemode) {
		engine.setRemainingMoves(mainModule.remainingMoves);
		mainModule.startSearch(false);
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::onEndSearch(ChessMove bm,
	ChessMove pm, [[maybe_unused]] int depth, [[maybe_unused]] int ply,
	[[maybe_unused]] std::uint64_t cntnodes, 
	[[maybe_unused]] std::uint64_t searchtime)
{
	if (mainModule.pondernextmove) {
		// take back if we were pondering
		engine.takeBack();
	} else {
		// play the move (if not in analyze mode)
		if ((engine.getSearchMode() != SearchMode::infinite_search) &&
			(!bm.isEmpty())) {
			engine.playMove(bm);
		}
	}

	// pondering canceled (stopped search or disable pondering)
	if (mainModule.pondernextmove) {

		mainModule.pondernextmove = false;

		if (mainModule.ponderingActive) {

			// play the move
			if (!engine.setMoveSquareList(pondernewmove)) {
				return;
			}

			// restart search if opponent played the wrong move
			mainModule.startSearch(false);
		}

	} else {

		if (!engine.isRunningTestSuite()) {

			mainModule.movePonder = pm;

			// update number of moves
			if (mainModule.maxGameMoves != 0) {
				--mainModule.remainingMoves;
				if (mainModule.remainingMoves == 0) {
					mainModule.remainingMoves += mainModule.maxGameMoves;
				}
			}

			mainModule.sendCommand("move " + engine.getTextMove(bm));

			if (mainModule.ponderingActive) {

				mainModule.pondernextmove = false;

				if (!mainModule.movePonder.isEmpty()) {

					std::string strmove = 
						engine.getTextMove(mainModule.movePonder);
					mainModule.sendCommand("Hint: " + strmove);

					if (engine.setMoveSquareList(strmove)) {
						mainModule.pondernextmove = true;
						mainModule.startSearch(true);
					}
				}
			}
        }
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::onSendMultiPV([[maybe_unused]] int num, int depth,
	int score, const ChessPVMoveList &mlist, int count, std::uint64_t cntnodes,
	std::uint64_t searchtime, [[maybe_unused]] int maxply,
	[[maybe_unused]] int bound, [[maybe_unused]] std::uint64_t tbhits)
{
	if (showthinking) {
		mainModule.sendCommand(std::to_string(depth) + ' ' +
						   std::to_string(score) + ' ' +
						   std::to_string(static_cast<int>(searchtime / 10)) +
						   ' ' + std::to_string(cntnodes) + ' ' +
						   engine.getTextPV(mlist, count));
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolCECP::onSendInfoMessage(const std::string &msg)
{
	mainModule.sendCommand(msg);
}

} // namespace Cheese
