////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "protocoluci.h"

#include "util/logfile.h"
#include "board.h"
#include "engine.h"
#include "module.h"
#include "tablebases.h"

#include <iomanip>
#include <string>

namespace Cheese {

const std::array<std::string, 3> strBound = { "", "upperbound ",
	"lowerbound " };

const std::array<std::string, 2> strMate = { "cp", "mate" };

////////////////////////////////////////////////////////////////////////////////
ChessProtocolUCI::ChessProtocolUCI(ChessEngine &e, ChessModule &m) :
	ChessProtocol(e, m)
{
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::init()
{
	auto &cfg = mainModule.getConfig();

	mainModule.sendCommand("id name " + ChessModule::name_program + ' ' +
		std::string(Cheese::project_ver));
	mainModule.sendCommand("id author " + ChessModule::name_author);
	mainModule.sendCommand("option name Hash type spin default "
		+ std::to_string(cfg.hashSize) +
		" min " + std::to_string(hashtable_size_min) +
		" max " + std::to_string(hashtable_size_max));
	mainModule.sendCommand("option name Ponder type check default false");
	mainModule.sendCommand("option name OwnBook type check default " +
		std::string(cfg.useBook ? "true" : "false"));
	mainModule.sendCommand("option name Book type string default " +
		cfg.bookFile);
	mainModule.sendCommand("option name Clear Hash type button");
	mainModule.sendCommand("option name UsePersonality type check default " +
		std::string(cfg.usePersonality ? "true" : "false"));
	mainModule.sendCommand("option name PersonalityFile type string default " +
		cfg.personalityFile);
	mainModule.sendCommand("option name UCI_Chess960 type check default false");
	mainModule.sendCommand("option name MultiPV type spin default 1 min 1 max " 
		+ std::to_string(max_multipv));
	mainModule.sendCommand("option name NullMovePruning type check default " +
		std::string(cfg.nullMoves ? "true" : "false"));
	mainModule.sendCommand("option name LateMoveReduction type check default " 
		+ std::string(cfg.lmr ? "true" : "false"));
	mainModule.sendCommand("option name Threads type spin default " +
		std::to_string(cfg.threads) + " min 1 max " +
		std::to_string(max_threads));
	mainModule.sendCommand("option name SMPMode type combo default " +
		cfg.smpMode + " var ybwc var lazy");
	mainModule.sendCommand("option name UCI_LimitStrength type check default " 
		+ std::string(cfg.useStrength ? "true" : "false"));
	mainModule.sendCommand("option name UCI_Elo type spin default " +
		std::to_string(cfg.elo) + " min " +
		std::to_string(min_strength_elo) + " max " +
		std::to_string(max_strength_elo));
	std::string strlimitmode = (cfg.uciLimitMode == ELOLimitMode::speed) ? 
		"speed" : "nodes";
	mainModule.sendCommand("option name LimitStrengthMode type combo default "
		+ strlimitmode + " var speed var nodes");
	mainModule.sendCommand("option name DrawScore type spin default " +
		std::to_string(cfg.drawScore) + " min -100 max 100");
	mainModule.sendCommand("option name SyzygyPath type string default " +
		cfg.pathTableBase);
	mainModule.sendCommand("option name SyzygyProbeDepth type spin default " +
		std::to_string(cfg.tbProbeDepth) + " min 1 max 100");
	mainModule.sendCommand("option name Syzygy50MoveRule type check default " +
		std::string((cfg.tb50MoveRule == 1) ? "true" : "false"));
	mainModule.sendCommand("option name SyzygyProbeLimit type spin default " +
		std::to_string(cfg.tbProbeLimit) + " min 0 max 7");

	mainModule.initEngine();

	mainModule.sendCommand("uciok");
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolUCI::parse(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return true;
	}

	if (token == "go") {

		parseGo(stream);
		mainModule.startSearch(mainModule.pondernextmove);

	} else
	if (token == "position") {

		parsePosition(stream);

	} else
	if (token == "isready") {

		mainModule.sendCommand("readyok");

	} else
	if (token == "ponderhit") {

		// the user has played the expected move
		if (engine.isThinking()) {
			// => continue the search or play
			engine.ponderHit();
		}

	} else
	if (token == "setoption") {
	    parseOption(stream);
	} else
	if (token == "quit") {
		return false;

	} else
	if (token == "ucinewgame") {

		// turn off pondering
		mainModule.movePonder.clear();

		if (engine.isThinking()) {
			engine.stopThinking();

			engine.newGame();

            mainModule.remainingMoves = 30;
            mainModule.maxGameMoves = 30;
            engine.setRemainingMoves(mainModule.remainingMoves);

		} else {
			engine.newGame();
		}

	} else
	if (token == "stop") {
		// stop thinking
		// if we are pondering, the opponent played an unexpected move
		if (engine.isThinking()) {
			engine.stopThinking();
		}
	} else
	if (!mainModule.parseModuleCommand(token, stream)) {
		Logger::debug() << "unknown parse command : " << token;
	}

	return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolUCI::parseOption(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return false;
	}

    if (token == "name") {

		auto &cfg = mainModule.getConfig();	

		if (!nextToken(stream, token)) {
			return false;
		}

		std::string name = token;
		if (!nextToken(stream, token)) {
			return false;
		}

		if (token != "value") {
			return false;
		}

		if (!nextToken(stream, token)) {
			return false;
		}

		if (name == "Hash") {

			engine.setHashTableSize(ChessModule::parseInt(token));

		} else
		if (name == "Ponder") {

			bool b = (token == "true");
			mainModule.ponderingActive = b;

		} else
		if (name == "NullMovePruning") {
			cfg.nullMoves = (token == "false") ? false : true; 
			engine.enableNullMoves(cfg.nullMoves);

		} else
		if (name == "LateMoveReduction") {
			cfg.lmr = (token == "false") ? false : true;
			engine.enableLMR(cfg.lmr);

		} else
		if (name == "OwnBook") {

			if (token == "true") {
				cfg.useBook = true;
				if (!engine.loadOpeningBook(cfg.bookFile)) {
					mainModule.sendCommand("info string book file not found");
					cfg.useBook = false;
				}
			} else {
				cfg.useBook = false;
				engine.unloadOpeningBook();
			}

		} else
		if (name == "Book") {

			cfg.bookFile.assign(token);
			if (cfg.useBook) {
				if (!engine.loadOpeningBook(cfg.bookFile)) {
					mainModule.sendCommand("info string book file not found");
					cfg.useBook = false;
				}
			}

		} else
		if (name == "Clear") {

			if (token == "Hash") {
				engine.clearHashTable();
			}

		} else
		if (name == "MultiPV") {

			engine.setMultiPV(ChessModule::parseInt(token));

		} else
		if (name == "UCI_Elo") {

			int ve = ChessModule::parseInt(token);
			if (cfg.useStrength) {
				engine.setStrengthELO(ve);
			}
			cfg.elo = ve;

		} else
		if (name == "UCI_LimitStrength") {

			if (token == "true") {
				engine.setStrengthELO(cfg.elo);
				cfg.useStrength = true;
			} else {
				engine.setStrengthELO(max_strength_elo);
				cfg.useStrength = false;
			}

		} else
		if (name == "LimitStrengthMode") {
			cfg.uciLimitMode = (token == "speed") ? ELOLimitMode::speed :
 				ELOLimitMode::nodes;
			engine.setELOLimitMode(cfg.uciLimitMode);
			if (cfg.useStrength) {
				engine.setStrengthELO(cfg.elo);
			}
		} else
		if (name == "SyzygyPath") {
			TableBases::init(token);
		} else
		if (name == "SyzygyProbeDepth") {
			engine.setTBProbeDepth(ChessModule::parseInt(token));
		} else
		if (name == "Syzygy50MoveRule") {
			engine.setTB50MoveRule((token == "true") ? true : false);
		} else
		if (name == "SyzygyProbeLimit") {
			engine.setTBProbeLimit(ChessModule::parseInt(token));
		} else
		if (name == "DrawScore") {

			engine.setDrawValue(ChessModule::parseInt(token));

		} else
		if (name == "UCI_Chess960") {

			engine.getBoardRef().setChessRules((token == "true") ?
				ChessRules::frc : ChessRules::classic);

		} else
		if (name == "SMPMode") {

			if (token != cfg.smpMode) {

				if (token == "ybwc") {
					engine.setSMPMode(SMPMode::ybwc);
				} else
				if (token == "lazy") {
					engine.setSMPMode(SMPMode::lazy);
				} else {
					return false;
				}

				cfg.smpMode = token;
				engine.setMaxThreads(cfg.threads);

				Logger::info() << "SMPMode = " << token;
			}
		} else
		if (name == "Threads") {

			int ve = ChessModule::parseInt(token);
			if (cfg.threads != ve) {
				engine.setMaxThreads(ve);
				cfg.threads = ve;
				Logger::info() << "Using " << ve << " threads";
			}

		} else
		if (name == "UsePersonality") {

			if (token == "true") {
				cfg.usePersonality = true;
				engine.setPersonality(cfg.personalityFile);
			} else {
				cfg.usePersonality = false;
				engine.setPersonality("");
			}

		} else
		if (name == "personalityFile") {

			cfg.personalityFile.assign(token);
			if (cfg.usePersonality) {
				engine.setPersonality(cfg.personalityFile);
			} else {
				engine.setPersonality("");
			}
		}

    } else {
		Logger::debug() << "unknown parse option " << token;
        return false;
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////
bool ChessProtocolUCI::parsePosition(std::istringstream &stream)
{
	std::string token;
	if (!nextToken(stream, token)) {
		return false;
	}

	if (token == "startpos") {

		// start position
		engine.setStartPosition();

        if (nextToken(stream, token)) {
			if (token == "moves") {
				if (streamString(stream, token)) {
					if (!engine.setMoveSquareList(token)) {
						Logger::debug() << "bad move !?";
					}
				}
			}
		}

	} else
	if (token == "fen") {

        if (!streamString(stream, token)) {
			return false;
		}

		auto p = token.find("moves");
		if (p != std::string::npos) {

			std::string strfen = token.substr(0, p);
			auto p2 = token.find_first_of(' ', p);
			auto p3 = token.find_first_not_of(' ', p2);

			std::string strmoves = token.substr(p3);

			if (!engine.setFENPosition(strfen)) {
				Logger::debug() << "Bad FEN string !";
				return false;
			}

			if (!engine.setMoveSquareList(strmoves)) {
				Logger::debug() << "bad move !?";
			}

		} else {
			if (!engine.setFENPosition(token)) {
				Logger::debug() << "Bad FEN string !";
				return false;
			}
		}

	} else {
		Logger::debug() << "unknown parse position " << token;
    }

	return true;
}

////////////////////////////////////////////////////////////////////////////////
// parse 'go' command
//
// go wtime 300000 btime 300000 winc 2 binc 2
// go movestogo 40 btime 300000 wtime 300000 winc 0 binc 0
//
bool ChessProtocolUCI::parseGo(std::istringstream &stream)
{
	std::string token;

	mainModule.pondernextmove = false;
    engine.setRemainingMoves(0);
	engine.setTimeIncrement(white, 0);
	engine.setTimeIncrement(black, 0);

    while (nextToken(stream, token)) {

		if (token == "ponder") {
			mainModule.pondernextmove = true;
		} else
        if (token == "wtime") {
            if (nextToken(stream, token)) {
				engine.setPlayerTime(white, ChessModule::parseInt(token));
				engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
			}
        } else
        if (token == "btime") {
            if (nextToken(stream, token)) {
				engine.setPlayerTime(black, ChessModule::parseInt(token));
				engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
			}
        } else
        if (token == "winc") {
            if (nextToken(stream, token)) {
				engine.setTimeIncrement(white, ChessModule::parseInt(token));
			}
        } else
        if (token == "binc") {
            if (nextToken(stream, token)) {
				engine.setTimeIncrement(black, ChessModule::parseInt(token));
			}
        } else
		if (token == "movetime") {
			if (nextToken(stream, token)) {
				engine.setEngineSearchMode(SearchMode::fixed_time,
					ChessModule::parseInt(token), 0);
			}
        } else
        if (token == "movestogo") {
            if (nextToken(stream, token)) {
				engine.setRemainingMoves(ChessModule::parseInt(token));
				engine.setEngineSearchMode(SearchMode::tournament, 0, 0);
			}
        } else
		if (token == "infinite") {
            engine.setEngineSearchMode(SearchMode::infinite_search, 0, 0);
        } else
		if (token == "depth") {
            if (nextToken(stream, token)) {
				engine.setEngineSearchMode(SearchMode::fixed_depth,
					ChessModule::parseInt(token), 0);
			}
        } else
        if (token == "nodes") {
            if (nextToken(stream, token)) {
				engine.setEngineSearchMode(SearchMode::fixed_nodes,
					ChessModule::parseInt(token), 0);
			}
        }
    }

	return true;
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onEndSearch(ChessMove bm,
	ChessMove pm, [[maybe_unused]] int depth, [[maybe_unused]] int ply,
	[[maybe_unused]] std::uint64_t cntnodes, 
	[[maybe_unused]] std::uint64_t searchtime)
{
	// don't need to play moves with UCI ?
	if (mainModule.pondernextmove) {
		engine.takeBack();
	} else {
		if (engine.getSearchMode() != SearchMode::infinite_search) {
			engine.playMove(bm);
		}
	}

	// pondering canceled (stopped search or disable pondering)
	if (mainModule.pondernextmove) {

		if (mainModule.ponderingActive) {
			std::string str = "bestmove " + engine.getTextMove(bm);
			if (!pm.isEmpty()) {
				str += " ponder " + engine.getTextMove(pm);
			}
			mainModule.sendCommand(str);
		} else {
			mainModule.pondernextmove = false;
		}

	} else {

		mainModule.movePonder = pm;

		std::string str = "bestmove " + engine.getTextMove(bm);
		if ((mainModule.ponderingActive) && (!pm.isEmpty())) {
			str += " ponder " + engine.getTextMove(pm);
		}
		mainModule.sendCommand(str);
	}
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendMultiPV(int num, int depth, int score,
	const ChessPVMoveList &mlist, int count, std::uint64_t cntnodes, 
	std::uint64_t searchtime, int maxply, int bound,
	[[maybe_unused]] std::uint64_t tbhits)
{
	std::uint64_t nps = (cntnodes * 1000) / (searchtime + 1);

	int sc = score;
	int im = 0;

	if (score <= min_score_mate) {
		im = 1;
		sc = -ChessEngine::calcMateValue(score);
	} else
	if (score >= max_score_mate) {
		im = 1;
		sc = ChessEngine::calcMateValue(score);
	}

	std::string str = "info";

	if (num != 0) {
		str += " multipv " + std::to_string(num);
	}

	str += " depth " + std::to_string(depth) +
		   " seldepth " + std::to_string(maxply) +
		   " score " + strMate[im] + ' ' + std::to_string(sc) +
		   ' ' + strBound[bound] +
		   "time " + std::to_string(searchtime) +
		   " nodes " + std::to_string(cntnodes) +
		   " nps " + std::to_string(nps) +
		   " tbhits " + std::to_string(tbhits) +
		   " pv " + engine.getTextPV(mlist, count);

	mainModule.sendCommand(str);
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendHashFull(std::uint64_t v)
{
	mainModule.sendCommand("info hashfull " + std::to_string(v));
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendInfoDepth(int d)
{
	mainModule.sendCommand("info depth " +  std::to_string(d));
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSearchRootMove(ChessMove move, int num)
{
	mainModule.sendCommand("info currmove " + engine.getTextMove(move) +
					    " currmovenumber " + std::to_string(num));
}

////////////////////////////////////////////////////////////////////////////////
void ChessProtocolUCI::onSendInfoMessage(const std::string &msg)
{
	mainModule.sendCommand("info string " + msg);
}

} // namespace Cheese
