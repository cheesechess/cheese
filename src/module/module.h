////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_MODULE_H_
#define CHEESE_MODULE_H_

#include "config.h"

#include "util/configfile.h"
#include "engine.h"
#include "listener.h"
#include "protocol.h"
#include "protocoluci.h"
#include "protocolcecp.h"

#include <sstream>
#include <string>
#include <vector>

namespace Cheese {

// GUI protocols
enum class GuiProtocol {

	// unknown protocol (default)
	none,

	// UCI protocol
	uci,

	// CECP protocol (xboard)
	cecp
};

////////////////////////////////////////////////////////////////////////////////
// options loaded from config file
struct ChessModuleConfig {

	// SMP algorithm : ybwc or lazy
	std::string smpMode;

	std::string bookFile;

	std::string personalityFile;

	std::string pathTableBase;

	// default hash size
	int hashSize;

	// default strength in ELO
	int elo;

	// default max number of threads
	int threads;

	// minimum split depth
	int minSplitDepth;

	// default log levels
	int logLevel;

	// contempt factor
	int drawScore;

	int tb50MoveRule;

	int tbProbeLimit;

	int tbProbeDepth;

	// 0 = reduce speed, 1 = recuce nodes
	ELOLimitMode uciLimitMode;

	// default use of opening book
	bool useBook;

	// default use of strength
	bool useStrength;

	// use personality file
	bool usePersonality;

	// use null moves pruning
	bool nullMoves;

	// use LMR
	bool lmr;

};

////////////////////////////////////////////////////////////////////////////////
// Chess module is the main object, to use chess engine with command line
// or gui
class ChessModule : public ChessEngineListener {

public:

	ChessEngine engine;

	std::unique_ptr<ChessProtocol> protocol;

	// configuration options
	ChessModuleConfig cfg;

	// current ponder move
	ChessMove movePonder;

	GuiProtocol guiMode {GuiProtocol::none};

	Logger::LogLevel level_gui { Logger::LogLevel::custom };

	int remainingMoves {0};

	int maxGameMoves {0};

	bool quit {false};

	// if pondering option is enable
	bool ponderingActive {false};

	// if we are pondering
	bool pondernextmove {false};

	static const std::string name_archi;

	static const std::string name_author;

	static const std::string name_program;


	ChessModule() = default;

	bool init(const std::vector<std::string> &args);

	void initEngine();

	// main loop
	void run();

	void free();

	void setProtocol(GuiProtocol p);

	ChessModuleConfig &getConfig();

	// send a command string to gui
	void sendCommand(const std::string &str) const;

	void doPerftd(int depth);

	void doPerft(int depth);

	void doDivide(int depth);

	void startSearch(bool ponder);

	// parse special commands available in all protocols
	bool parseModuleCommand(const std::string &token, 
		std::istringstream &stream);

	// convert string to int
	// return 0 if the string is not a number
	static int parseInt(const std::string &str);

	// convert string to double
	// return 0.0 if the string is not a double
	static double parseDouble(const std::string &str);

private:

	void parse();

	bool parseDefault(std::istringstream &stream);

	bool parseArguments(const std::vector<std::string> &args, bool &err);

	bool loadConfig();

	void printHelp();

	bool cmdRunTestSuite(const std::vector<std::string> &args);

	bool cmdPerftd(const std::vector<std::string> &args);

	bool cmdPerft(const std::vector<std::string> &args);

	bool cmdDivide(const std::vector<std::string> &args);

	bool cmdProbeTB(const std::vector<std::string> &args);

	bool cmdProbeBook(const std::vector<std::string> &args);

	bool cmdBenchmark(const std::vector<std::string> &args);

	void onEndSearch(ChessEngineNotifier &notifier,
		ChessMove bm, ChessMove pm, int depth, int ply, std::uint64_t cntnodes,
		std::uint64_t searchtime) override;

	void onSendMultiPV(ChessEngineNotifier &notifier, int num, int depth,
		int score, const ChessPVMoveList &mlist, int count, 
		std::uint64_t cntnodes, std::uint64_t searchtime, int maxply, int bound, 
		std::uint64_t tbhits) override;

	void onSendHashFull(ChessEngineNotifier &notifier, 
		std::uint64_t v) override;

	void onSendInfoDepth(ChessEngineNotifier &notifier, int d) override;

	void onSearchRootMove(ChessEngineNotifier &notifier, ChessMove move,
		int num) override;

	void onSendInfoMessage(ChessEngineNotifier &notifier,
		const std::string &msg) override;
};

////////////////////////////////////////////////////////////////////////////////
inline ChessModuleConfig &ChessModule::getConfig()
{
	return cfg;
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onEndSearch(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	ChessMove bm, ChessMove pm, int depth, int ply,
	std::uint64_t cntnodes, std::uint64_t searchtime)
{
	protocol->onEndSearch(bm, pm, depth, ply, cntnodes, searchtime);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendMultiPV(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	int num, int depth, int score, const ChessPVMoveList &mlist, int count,
	std::uint64_t cntnodes, std::uint64_t searchtime, int maxply, int bound,
	std::uint64_t tbhits)
{
	protocol->onSendMultiPV(num, depth, score, mlist, count, cntnodes,
		searchtime, maxply, bound, tbhits);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendHashFull(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	std::uint64_t v)
{
	protocol->onSendHashFull(v);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendInfoDepth(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	int d)
{
	protocol->onSendInfoDepth(d);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSearchRootMove(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	ChessMove move, int num)
{
	protocol->onSearchRootMove(move, num);
}

////////////////////////////////////////////////////////////////////////////////
inline void ChessModule::onSendInfoMessage(
	[[maybe_unused]] ChessEngineNotifier &notifier,
	const std::string &msg)
{
	protocol->onSendInfoMessage(msg);
}

} // namespace Cheese

#endif //CHEESE_MODULE_H_
