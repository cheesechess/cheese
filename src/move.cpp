////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#include "move.h"

#include "util/logfile.h"

#include <iomanip>

namespace Cheese {

const std::array<char, nb_pieces_name> pieces_name = {
	' ', 'p', 'n', 'b', 'r', 'q', 'k', ' ',
	' ', 'P', 'N', 'B', 'R', 'Q', 'K', ' '
};

////////////////////////////////////////////////////////////////////////////////
constexpr auto makeCharFile(File x)
{
	constexpr auto f = static_cast<int>('a');
	return static_cast<char>(f + static_cast<int>(x));
}

////////////////////////////////////////////////////////////////////////////////
constexpr auto makeCharRank(Rank y)
{
	constexpr auto r = static_cast<int>('1');
	return static_cast<char>(r + static_cast<int>(y));
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::textShort(const ChessMoveSort *moves, int count) const
{
	std::string str;

	if (flag == 0) {
		return str;
	}

	Square sqsrc = src();
	Square sqdst = dst();
	File sx = squareFile(sqsrc);
	Rank sy = squareRank(sqsrc);
	File dx = squareFile(sqdst);
	Rank dy = squareRank(sqdst);
	Piece mp = piece();

	bool ss = false;
	bool sf = false;
	bool sr = false;

	// find if we have another piece of the same type able to move to
	// destination
	if ((mp != pawn) && (mp != king)) {
		for (int n=0; n<count; n++) {

			const ChessMove &move = moves[n].getMove();

			// regarde si on a la meme piece qui peut atteindre la destination
			if ((move.piece() == mp) &&
				(move.dst() == sqdst) &&
				(move.src() != sqsrc)) {

				ss = true;

				File sqx = squareFile(move.src());
				Rank sqy = squareRank(move.src());

				if (sqx == sx) {
					sf = true;
				}
				if (sqy == sy) {
					sr = true;
				}

				if ((!sf) && (!sr)) {
					sr = true;
				}
			}
		}
	}

	if (isCastle()) {
		str += 'O';
		str += '-';
		str += 'O';

		if (dx < sx) {
			str += '-';
			str += 'O';
		}
	} else {

		// source
		if (mp != pawn) {
			str += pieces_name[mp + piece_color_offset];
		}

		if (ss) {
			if (sr) {
				str += makeCharFile(sx);
			}
			if (sf) {
				str += makeCharRank(sy);
			}
		}

		// action
		if (isCapture()) {
			if (mp == 1) {
				str += makeCharFile(sx);
			}
			str += 'x';
		}

		// destination
		str += makeCharFile(dx);
		str += makeCharRank(dy);

		// promotion
		Piece pro = promotion();
		if (pro != no_piece) {
			str += '=';
			str += pieces_name[pro + piece_color_offset];
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::text() const
{
	std::string str;

	if (flag == 0) {
		return str;
	}

	Square sqsrc = src();
	Square sqdst = dst();
	File sx = squareFile(sqsrc);
	Rank sy = squareRank(sqsrc);
	File dx = squareFile(sqdst);
	Rank dy = squareRank(sqdst);

	if (isCastle()) {
		str += 'O';
		str += '-';
		str += 'O';
		if (dx < sx) {
			str += '-';
			str += 'O';
		}
	} else {

		// piece
		Piece mp = piece();
		if (mp != pawn) {
			str += pieces_name[mp + piece_color_offset];
		}

		// source
		str += makeCharFile(sx);
		str += makeCharRank(sy);

		// action
		if (isCapture()) {
			str += 'x';
		} else {
			str += '-';
		}

		// destination
		str += makeCharFile(dx);
		str += makeCharRank(dy);

		// promotion
		Piece pro = promotion();
		if (pro != no_piece) {
			str += '=';
			str += pieces_name[pro + piece_color_offset];
		}
	}

	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::strSquare(Square sq)
{
	std::string str;
	str += makeCharFile(squareFile(sq));
	str += makeCharRank(squareRank(sq));
	return str;
}

////////////////////////////////////////////////////////////////////////////////
std::string ChessMove::textMove() const
{
	Square sqsrc = src();
	Square sqdst = dst();

	std::string str;
	str += makeCharFile(squareFile(sqsrc));
	str += makeCharRank(squareRank(sqsrc));
	str += makeCharFile(squareFile(sqdst));
	str += makeCharRank(squareRank(sqdst));
	return str;
}

////////////////////////////////////////////////////////////////////////////////
void ChessMove::print() const
{
	Logger::debug() << "move (" << textMove() << " 0x"
			  << std::setfill('0') << std::setw(8) << std::hex << flag
			  << ") : src "
			  << strSquare(src())
			  << ", dst "
			  << strSquare(dst())
			  << ", mv (" << pieces_name[piece() + piece_color_offset] << ')'
			  << ", cap (" << pieces_name[capture() + piece_color_offset] << ')'
			  << ", pro (" << pieces_name[promotion() + piece_color_offset] 
			  << ')'
			  << ", cas " << isCastle()
			  << ", ep " << isEnPassant();
}

} // namespace Cheese
