////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_FEN_H_
#define CHEESE_FEN_H_

#include "config.h"

#include "board.h"

#include <string>
#include <vector>

namespace Cheese {

////////////////////////////////////////////////////////////////////////////////
// FEN (Forsythe-Edwards Notation)
//
// default position :
//		rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
//

////////////////////////////////////////////////////////////////////////////////
class ChessFENParser {

	ChessBoard &board;

public:

	explicit ChessFENParser(ChessBoard &b);

	// set board position from FEN string
	[[nodiscard]]
	bool set(const std::string &fen);

	// get FEN string from board
	[[nodiscard]]
	std::string get();

	// check if the FEN string is valid
	[[nodiscard]]
	bool check(const std::string &fen) const;

	[[nodiscard]]
	bool reverse(const std::string &str, std::string &newfen);

	[[nodiscard]]
	bool load(const std::string &filename, std::vector<std::string> &fens);

	[[nodiscard]]
	std::string getOption(const std::string &fen, const std::string &name);

private:

	void init();

	void end();

	void setPiece(int x, int y, int pc);

	void setSide(Side side);

	void setCastle(Side side, int type);

	void setCastleFRC(Side side, int f);

	void setEnPassant(int ep);

	void setMoves(int count);

	void setFifty(int fifty);

	[[nodiscard]]
	int getPiece(int x, int y);

	[[nodiscard]]
	Side getSide() const;

	[[nodiscard]]
	int getCastle(Side side, int type) const;

	[[nodiscard]]
	int getEnPassant() const;

	[[nodiscard]]
	int getMoves() const;

	[[nodiscard]]
	int getFifty() const;

};

} // namespace Cheese

#endif //CHEESE_FEN_H_
