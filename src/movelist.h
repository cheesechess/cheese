////////////////////////////////////////////////////////////////////////////////
//
// Cheese, Chess engine.
//
// Copyright (C) 2006-2024 Patrice Duhamel. All rights reserved.
//
// Cheese is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Cheese is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef CHEESE_MOVELIST_H_
#define CHEESE_MOVELIST_H_

#include "config.h"

#include "move.h"

#include <array>
#include <algorithm>

namespace Cheese {

// max number of moves each turn (maximum = 218)
inline constexpr int max_play_moves = 256;

////////////////////////////////////////////////////////////////////////////////
class ChessMoveSortList {

	std::array<ChessMoveSort, max_play_moves> moves;

	ChessMoveSort *pos;

public:

	ChessMoveSortList() : pos(moves.data())
	{}

	ChessMoveSortList & operator=(const ChessMoveSortList &mo);

	[[nodiscard]] 
	inline const auto *begin() const
	{
		return moves.data();
	}

	[[nodiscard]] 
	inline const auto *end() const
	{
		return pos;
	}

	[[nodiscard]] 
	inline auto *begin()
	{
		return moves.data();
	}

	[[nodiscard]] 
	inline auto *end()
	{
		return pos;
	}

	[[nodiscard]] 
	inline auto *data()
	{
		return moves.data();
	}

	[[nodiscard]] 
	inline int size() const
	{
		return static_cast<int>(pos - moves.data());
	}

	[[nodiscard]] 
	inline bool empty() const
	{
		return (pos == moves.data());
	}

	inline void clear()
	{
		pos = moves.data();
	}

	[[nodiscard]] 
	inline auto & operator[](int index)
	{
		return moves[index];
	}

	[[nodiscard]] 
	inline auto & operator[](int index) const
	{
		return moves[index];
	}

	inline void push(const ChessMoveSort &move)
	{
		*(pos++) = move;
	}

	inline void push(const ChessMove &move)
	{
		// update only the move (speed)
		(pos++)->setMove(move);
	}

	inline void setScore(int index, ChessMoveSort::value_type v)
	{
		moves[index].setScore(v);
	}

	void sort(int p, int cnt);

	void moveFront(const ChessMove &pvmove);

	void moveBestToFront(int p);

	void remove(const ChessMove &move);
};

} // namespace Cheese

#endif // CHEESE_MOVELIST_H_

